package com.example.grosd.festivalintranet2018android.Vue;
/**
 * Created by QG on 27/02/2019.
 */
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import android.widget.AdapterView;

import com.example.grosd.festivalintranet2018android.Controleur.Metier.Representation;
import com.example.grosd.festivalintranet2018android.R;

import java.util.ArrayList;

import static android.R.layout.simple_list_item_1;

public class liste_representations extends AppCompatActivity {

    private static final String REPRESENTATIONS_EMPTY  = "<representations><representation><nom>Vide!</nom></representation></representations>";

    // 10.0.2.2 : pour atteindre une ressource sur la machine hote de l'AVD
    private static final String URL_XMLRESSOURCE = "http://172.15.6.240/proj2eq1/Festival_intranet_2018_mobile/service_web_rest/getAllRepresentationsXML.php";


    private ListView listviewRepresentations;
    private ArrayList<Representation> lesRepresentations;
    private ReqHTTPTask reqHTTPTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_representations);


        this.listviewRepresentations = (ListView) findViewById(R.id.listviewRepresentations);
        lesRepresentations = new ArrayList<Representation>();
        this.listviewRepresentations.setAdapter(new ArrayAdapter<Representation>(this,simple_list_item_1, lesRepresentations));

        if (reqHTTPTask != null) {
            return;
        }

        reqHTTPTask = new ReqHTTPTask();
        reqHTTPTask.execute((Void) null);

        listviewRepresentations.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "Represenstation sélectionné : " +listviewRepresentations.getAdapter().getItem(position), Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("representations", listviewRepresentations.getAdapter().getItem(position).toString());
                bundle.putString("groupe", ((Representation)listviewRepresentations.getAdapter().getItem(position)).getGroupe());
                bundle.putString("lieu", ((Representation)listviewRepresentations.getAdapter().getItem(position)).getIdLieu());
                bundle.putString("date", ((Representation)listviewRepresentations.getAdapter().getItem(position)).getDateRepresentation());
                bundle.putString("heureDeb", ((Representation)listviewRepresentations.getAdapter().getItem(position)).getHeureDebut());
                bundle.putString("heureFin", ((Representation)listviewRepresentations.getAdapter().getItem(position)).getHeureFin());
                Intent detailIntent = new Intent(getApplicationContext(), Une_representation.class);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
            }
        });

    }

    // Début définition classe ReqHTTPTask
    public class ReqHTTPTask extends AsyncTask<Void, Void, Boolean> {

        // exécution du traitement dans un autre Thread que le thread principal (UI)
        // récupération des données XML et remplissage dans une l'ArrayList lesGroupes
        @Override
        protected Boolean doInBackground(Void... params) {
            // modification de la liste des groupes
            lesRepresentations.clear();
            UtilRepresentationsXML.parseListeRepresentations(URL_XMLRESSOURCE, REPRESENTATIONS_EMPTY, lesRepresentations);
            return true;
        }

        // exécution du traitement dans le thread principal (UI) : il agit sur les contrôles graphiques
        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                // affichage du nombre de groupes (50 normalement)
                Toast.makeText(getApplicationContext(),
                        "Nombre de representations :" + lesRepresentations.size(), Toast.LENGTH_LONG).show();
                // récupération du modèle de données de la ListView
                ArrayAdapter<Representation> modelDeListeRepresentations = (ArrayAdapter<Representation>) listviewRepresentations
                        .getAdapter();
                // envoi du signal que les données (lesGroupes) ont été modifiées
                modelDeListeRepresentations.notifyDataSetChanged();
                // affichage du message succès
                Toast.makeText(getApplicationContext(), "Succès : " + success,
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "erreur de chargement des representations", Toast.LENGTH_LONG).show();
            }
        }
    } // fin définition classe ReqHTTPTask

}
