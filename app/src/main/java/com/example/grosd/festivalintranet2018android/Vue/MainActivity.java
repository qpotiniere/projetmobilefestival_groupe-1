package com.example.grosd.festivalintranet2018android.Vue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.grosd.festivalintranet2018android.R;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void listerepresentations(View view)
    {
        startActivity(new Intent(this, liste_representations.class));

    }
}
