package com.example.grosd.festivalintranet2018android.Controleur.Metier;

import android.support.annotation.FontRes;

/**
 * Created by QG on 27/02/2019.
 */
public class Representation {
    private int id;
    private String dateRepresentation;
    private String lieu;
    private String groupe;
    private String heureDebut;
    private String heureFin;

    public Representation(int id, String dateRepresentation, String heureDebut, String heureFin, String lieu, String groupe ) {
        this.id = id;
        this.dateRepresentation = dateRepresentation;
        this.lieu = lieu;
        this.groupe = groupe;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }
    public Representation() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public  String getDateRepresentation() {
        return dateRepresentation;
    }

    public void setDateRepresentation(String dateRepresentation) {
        this.dateRepresentation = dateRepresentation;
    }

    public String getIdLieu() {
        return lieu;
    }

    public void setIdLieu(String lieu) {
        this.lieu = lieu;
    }

    public  String getGroupe() {
        return groupe;
    }

    public void setIdGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
    }

    public String getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(String heureFin) {
        this.heureFin = heureFin;
    }

    @Override
    public String toString() {
        return  dateRepresentation + '\n' +
                "Groupe = " + groupe + '\n';
    }

}
