package com.example.grosd.festivalintranet2018android.Vue;
/**
 * Created by QG on 27/02/2019.
 */
import android.util.Xml;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import com.example.grosd.festivalintranet2018android.Controleur.Metier.Representation;

import org.xmlpull.v1.XmlPullParser;

public class UtilRepresentationsXML {
    static public void parseListeRepresentations(String urlXmlressource, String defaultContent, ArrayList<Representation> dest) {
        XmlPullParser parser = Xml.newPullParser();


        try {

            // auto-detect the encoding from the stream
            parser.setInput(getInputStream(urlXmlressource, defaultContent), null);

            int eventType = parser.getEventType();
            Representation currentRepresentation = null;
            boolean done = false;
            while (eventType != XmlPullParser.END_DOCUMENT && !done) {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("representation")) {
                            currentRepresentation = new Representation();
                        } else if (currentRepresentation != null) {
                            if (name.equalsIgnoreCase("id")) {
                                currentRepresentation.setId(Integer.parseInt(parser.nextText()));
                            } else if (name.equalsIgnoreCase("date_representation")) {
                                currentRepresentation.setDateRepresentation(parser.nextText());
                            }  else if (name.equalsIgnoreCase("heure_debut")) {
                                currentRepresentation.setHeureDebut(parser.nextText());
                            }  else if (name.equalsIgnoreCase("heure_fin")) {
                                currentRepresentation.setHeureFin(parser.nextText());
                            }  else if (name.equalsIgnoreCase("lieu")) {
                                currentRepresentation.setIdLieu(parser.nextText());
                            }  else if (name.equalsIgnoreCase("groupe")) {
                                currentRepresentation.setIdGroupe(parser.nextText());
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        // si fin de balise country, on ajoute l'objet dans la liste
                        if (name.equalsIgnoreCase("representation") && currentRepresentation != null) {
                            Log.i("ajout representation : ", currentRepresentation.toString());
                            dest.add(currentRepresentation);
                        }
                        // fin des representations ?
                        else if (name.equalsIgnoreCase("representations")) {
                            // oui, on signe l'arrêt de la boucle
                            done = true;
                        }
                        break;
                }
                // avance dans le flux
                eventType = parser.next();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static private InputStream getInputStream(String strUrl, String defaulContent) {
        InputStream res = null;
        try {
            Log.i("url", strUrl);
            URL url = new URL(strUrl);
            res = url.openConnection().getInputStream();
        } catch (Exception e) {
            Log.d("Erreur de lecture", strUrl + ":"+ e.getMessage());
        }
        if (res == null)
            // retourne quelque chose tout de même...
            res = new ByteArrayInputStream(defaulContent.getBytes());
        return res;
    }
}
