package com.example.grosd.festivalintranet2018android.Vue;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


import com.example.grosd.festivalintranet2018android.Controleur.Metier.Representation;
import com.example.grosd.festivalintranet2018android.R;

public class Une_representation extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vue_une_representation);

        String representations_groupe = getIntent().getExtras().getString("groupe");
        TextView tv_groupe = (TextView) findViewById(R.id.nom_groupe);
        tv_groupe.setText(representations_groupe);

        String representations_lieu = getIntent().getExtras().getString("lieu");
        TextView tv_lieu = (TextView) findViewById(R.id.nom_lieu);
        tv_lieu.setText(representations_lieu);

        String representations_date = getIntent().getExtras().getString("date");
        TextView tv_date = (TextView) findViewById(R.id.date_representation);
        tv_date.setText(representations_date);

        String representations_heureDeb = getIntent().getExtras().getString("heureDeb");
        TextView tv_heureDeb = (TextView) findViewById(R.id.heure_debut);
        tv_heureDeb.setText(representations_heureDeb);

        String representations_heureFin = getIntent().getExtras().getString("heureFin");
        TextView tv_heureFin = (TextView) findViewById(R.id.heure_fin);
        tv_heureFin.setText(representations_heureFin);
    }
}
